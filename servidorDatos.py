#!/usr/bin/env python3
#simple servidor de datos REST API
import datetime
from config import mongoDbData as DbD
from pymongo import MongoClient
from bson.json_util import dumps
#import needed for the rpc json server
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher
import ssl

@dispatcher.add_method
def obtenerUltimos(*args): 
    '''
    funcion para obtener los ultimos N valores
    en datosSistema
    '''
    lista=[]
    client = MongoClient(DbD['host'],DbD['port'])
    db = client.test
    #if no arguments:
    if len(args) == 0:
        lista  = 'nada por hacer, porfavor envia parametros'
    #si tiene un argumento
    elif len(args) == 1:
        lista = db.datosSistema.find().limit(args[0])
    #si tiene una lista de argumentos
    else:
        for arg in args:
            lista = db.datosSistema.find().limit(arg)
    return dumps(lista)

@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}

    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    ctx = ssl.SSLContext(ssl.PROTOCOL_SSLv23) 
    ctx.load_cert_chain('ssl.cert', 'ssl.key')
    run_simple('0.0.0.0', 54321, application, ssl_context = ctx)

