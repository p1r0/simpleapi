#!/bin/bash
openssl genrsa 4096 > ssl.key
openssl req -new -x509 -nodes -sha256 -days 365 -key ssl.key > ssl.cert
