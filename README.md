# simpleAPI

**This is a simple API  code that can help for bigger projects (example linked with mongodb). Integrates SSL and encourage you to use it for secure connections**

## Requirements

* python >= 3.5.3
* certifi==2018.8.24
* chardet==3.0.4
* idna==2.7
* json-rpc==1.11.1
* pkg-resources==0.0.0
* pymongo==3.4.0
* requests==2.19.1
* urllib3==1.23
* Werkzeug==0.14.1

## Instalation

**First you need to have the python base system requirements**
```sh
$ sudo apt-get install build-essential python3 python3-dev python3-setuptools python3-pip virtualenv
```
**Secondly you need to install mongodb for use the example code (OPTIONAL)**
```sh
$ sudo apt-get install mongodb
```
**MongoDB store some example data**
```sh
$ mongo
```
write some sample data:
```sql
>use test
>db.datosSistema.insertOne({'Name': 'p1r0', 'email': 'daroperez_negron@esimez.mx', 'generated': Date()})
```
**clone the repo and switch to simpleAPI directory:**
```sh
$ git clone https://gitlab.com/p1r0/simpleapi.git
$ cd simpleapi
```
**set the virtual enviroment**
```sh
$ virtualenv -p python3 myenv
$ source myenv/bin/activate
```

**install requirements.txt into the virtualenv:**
```sh
(myenv)$ pip3 install -r requirements.txt
```
**create a ssl certificate (run the createcert.sh script)**
```sh 
(myenv)$ ./createcert.sh
Country Name (2 letter code) [AU]:MX
State or Province Name (full name) [Some-State]:MICH
Locality Name (eg, city) []:MORELIA
Organization Name (eg, company) [Internet Widgits Pty Ltd]:NH
Organizational Unit Name (eg, section) []:Security
Common Name (e.g. server FQDN or YOUR name) []:127.0.0.1
Email Address []:p1r0@127.0.0.1
```
> NOTE:
> for testing purposes we use the Common Name as 127.0.0.1
> so that we have no problems testing it locally.
> In most cases, this should be a domain name linked with the certificate

**Run the server:**
```sh
(myenv)$ python3 servidorDatos.py
```
**In a second terminal run the client and get the data**
```sh
$ source myenv/bin/activate
(myenv)$ python3 clienteDatos.py
```
*lets hope you get the output or something like this:*
```sh
1
[{'_id': ObjectId('5bae4abc25ceb201f3390c35'), 'generated': 'Fri Sep 28 2018 15:37:32 GMT+0000 (UTC)', 'Name': 'p1r0', 'email': 'daroperez_negron@esimez.mx'}]
```
> ;)

***
# License

[**GPLV3**](./LICENSE).

# Information and contacts.

***Developers***

- p1r0 [daroperez_negron@esimez.mx](mailto:daroperez_negron@esimez.mx)

### ToDo

 - Integrate more databases and/or cryptocurrencies daemons.

## References:

[**SSL in the Server**](http://werkzeug.pocoo.org/docs/0.14/serving/)

[**SSL in Client**](http://steven.casagrande.io/articles/python-requests-and-ssl/)

