#!/usr/bin/env python3
#The Verge Lotto ticket generator rpc client

import requests
#from json import dumps, loads
from bson.json_util import dumps, loads

def main():
    url = "https://127.0.0.1:54321/jsonrpc"
    headers = {'content-type': 'application/json'}
    verify = "ssl.cert"

    # Example ticketGen method
    payload = {
        "method": "obtenerUltimos",
        "params": [30],
        "jsonrpc": "2.0",
        "id": 1,
    }
    response = requests.post(
        url, data=dumps(payload), headers=headers,
        verify=verify).json()
    response = loads(response['result'])
    print(len(response))
    print(response)

if __name__ == "__main__":
        main()

